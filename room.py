from __future__ import print_function
import copy


class Room:
    def __init__(self, data, rooms):
        self.data = data
        self.rooms = rooms
        self.players = []
        self.items = copy.deepcopy(data['items'])

    def look_at_room(self, looking_player):
        response = self.data['room_name'] + '\n'
        response += self.data['room_description'] + '\n'
        response += 'Exits: [' + ', '.join(self.data['exits'].keys()) + ']\n'

        for player in self.players:
            if player != looking_player:
                response += player.username + " is standing here.\n"

        for item in self.items:
            response += "a {0} is sitting here on the ground\n".format(
                item['name'])
        response += "\n"

        looking_player.send_message(response)

    def reset_room(self):
        message = ''
        for item in self.items:
            message += "A {0} disolved into the ground.\n".format(item['name'])

        self.items = copy.deepcopy(self.data['items'])
        for item in self.items:
            message += "A {0} materializes from thin air.\n".format(
                item['name'])

        if message != '':
            self.send_to_room(None, message)

    def send_to_room(self, sender, message):
        for player in self.players:
            if player != sender:
                player.send_message(message)

    def exits(self):
        return self.data['exits'].keys()

    def enter(self, sender):
        for player in self.players:
            if player != sender:
                player.send_message(sender.username + " entered the room.\n")
        self.players.append(sender)
        self.look_at_room(sender)
        sender.current_room = self

    def exit(self, sender, direction):
        if direction in self.data['exits']:
            if self.data['exits'][direction] in self.rooms:
                self.players.remove(sender)
                for player in self.players:
                    if player != sender:
                        player.send_message(
                            sender.username + " went " + direction + ".\n")
                self.rooms[self.data['exits'][direction]].enter(sender)
            else:
                sender.send_message(
                    "You attempt to go " + direction + ", but the path appears to go nowhere.\n")
