from __future__ import print_function

import socket
import select
import sys
from threading import *
from client import Client
from room_loader import load_rooms
from login import login
import commands
import time


class Server:
    def __init__(self, interface, port, connections):
        self.list_of_clients = []
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server.bind(('', 8081))
        self.server.listen(100)
        self.rooms = load_rooms()
        self.commands = commands.commands
        self.locked_names = []
        self.locked_name_lock = Lock()

    def lock_name(self, name):
        with self.locked_name_lock:
            if name in self.locked_names:
                return False
            else:
                self.locked_names.append(name)
                return True

    def release_name(self, name):
        with self.locked_name_lock:
            if name in self.locked_names:
                self.locked_names.remove(name)

    def is_logged_in(self, name):
        return any([client.username == name for client in self.list_of_clients])

    def run(self):
        t = Thread(target=self.heartbeat_thread)
        t.daemon = True
        t.start()

        t = Thread(target=self.room_reset_thread)
        t.daemon = True
        t.start()

        while True:
            conn, addr = self.server.accept()
            t = Thread(target=self.clientthread, args=(conn, addr,))
            t.daemon = True
            t.start()
        conn.close()
        self.server.close()

    def heartbeat_thread(self):
        while True:
            time.sleep(1)
            for client in self.list_of_clients:
                client.heartbeat()

    def room_reset_thread(self):
        while True:
            time.sleep(30)
            for room in self.rooms:
                self.rooms[room].reset_room()

    def clientthread(self, conn, addr):
        # connect setup goes here
        print(addr[0] + " connected")

        account_info = login(conn, self)

        if account_info is not None:
            print(addr[0] + " is connected as " + account_info['username'])
            client = Client(
                self, conn, account_info, self.rooms[1])
            self.list_of_clients.append(client)
            client.run()
            self.remove(client)

        print(addr[0] + " disconnected")

    def broadcast(self, message, sender):
        for client in self.list_of_clients:
            if client != sender:
                try:
                    client.send_message(message)
                except:
                    self.remove(client)

    def remove(self, client):
        if client in self.list_of_clients:
            self.list_of_clients.remove(client)
