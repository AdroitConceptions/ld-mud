import yaml
import os


def file_name(name):
    return os.path.join('users', name + '.yml')


def save_character(account_info):
    with open(file_name(account_info['username']), 'w') as outfile:
        yaml.dump(account_info, outfile, default_flow_style=False)


def read_character(account_name):
    try:
        with open(file_name(account_name)) as infile:
            return yaml.load(infile.read(), Loader=yaml.FullLoader)
    except Exception:
        pass

    return None
