from __future__ import print_function

NAME = 'help'

HELP_TEXT = 'Display this Message'

MOVEMENT_INFO = """
To move between rooms, type the exit name.
"""

MESSAGE_HEADER = """
Command     Description
-------     ------------
"""


def run(player, data):
    message = MESSAGE_HEADER
    for command in player.server.commands:
        message += "{0:10} - {1}\n".format(command.NAME, command.HELP_TEXT)

    message += MOVEMENT_INFO
    player.send_message(message)
