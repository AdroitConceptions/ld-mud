from __future__ import print_function
NAME = 'drop'

HELP_TEXT = 'drop <x> drop something on the ground'


def run(player, data):
    if data in [x['name'] for x in player.account_info['items']]:
        for i in range(len(player.account_info['items'])):
            if(player.account_info['items'][i]['name'] == data):
                item = player.account_info['items'].pop(i)
                player.current_room.items.append(item)
                player.send_message("You drop a {}\n".format(data))
                player.current_room.send_to_room(
                    player, "{0} drops a {1}.\n".format(player.username, data))
                break
    else:
        player.send_message(
            "You don't have a {}.\n".format(data))
