from __future__ import print_function
NAME = 'quit'

HELP_TEXT = 'quit the game'


def run(player, data):
    player.disconnect()
