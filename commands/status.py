from __future__ import print_function
NAME = 'status'

HELP_TEXT = 'check your status'

STATUS_TEMPLATE = """
Health:         {0}
Experiance:     {1}
Status Effects: {2}
Skills:         {3}
Items:          {4}
"""


def run(player, data):
    player.send_message(STATUS_TEMPLATE.format(
        player.account_info['health'],
        player.account_info['experiance'],
        ', '.join(["{0}: [{1}]".format(x[0], x[1])
                   for x in player.account_info['status_effects']]),
        ', '.join(player.account_info['skills']),
        ', '.join([x['name']
                   for x in player.account_info['items']]),
    ))
