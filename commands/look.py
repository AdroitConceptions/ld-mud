from __future__ import print_function
NAME = 'look'

HELP_TEXT = 'look <x> to look at something, no parameter to look at room'

LOOK_AT_PLAYER = """
{0} looks {1}
{0} is effected by {2}
{0} is carrying {3}

"""


def run(player, data):
    if data is None:
        player.current_room.look_at_room(player)
    else:
        for pl in player.current_room.players:
            if pl.username == data:

                player.send_message(LOOK_AT_PLAYER.format(
                    pl.username,
                    healthyness(pl.account_info['health']),
                    ', '.join(["{0}: [{1}]".format(x[0], x[1])
                               for x in pl.account_info['status_effects']]) if len(
                        pl.account_info['status_effects']) > 0 else "Nothing",
                    ', '.join([x['name']
                               for x in pl.account_info['items']]) if len(
                        pl.account_info['items']) > 0 else "Nothing",
                ))

                player.current_room.send_to_room(
                    player, "{0} looks at {1}\n".format(player.username, pl.username))
                break


def healthyness(health):
    if health > 90:
        return "Healthy"
    elif health > 50:
        return "Worn Down"
    elif health > 25:
        return "Bad"
    elif health > 0:
        return "At Death's Door"
    else:
        return "Dead"
