from __future__ import print_function

from os.path import dirname, basename, isfile
import glob

from importlib import import_module


modules = glob.glob(dirname(__file__)+"/*.py")
__all__ = [basename(f)[:-3] for f in modules if isfile(f)
           and not f.endswith('__init__.py')]

commands = []

for x in __all__:
    try:
        commands.append(import_module("." + x, "commands"))
    except ImportError as e:
        print("Error importing ", x, '.')
