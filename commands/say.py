from __future__ import print_function
NAME = 'say'

HELP_TEXT = 'talk to anyone in the room'


def run(player, data):
    print(player.username + ": " + data)
    message_to_send = player.username + ' says "' + data + '"\n'
    player.current_room.send_to_room(player, message_to_send)
