from __future__ import print_function
NAME = 'burp'

HELP_TEXT = 'burp, getting some on yourself'


def run(player, data):
    player.send_message("You burb LOUDLY and get some on yourself\n")
    found = False
    for effect in player.account_info['status_effects']:
        if effect[0] == 'Burb Bits':
            effect[1] += 30
            found = True
    if not found:
        player.account_info['status_effects'].append(['Burb Bits', 30])

    if player.account_info['health'] > 50:
        player.account_info['health'] = 50

    message_to_send = "{0} burbs LOUDLY and gets some on {0}'s self\n".format(
        player.username)
    player.current_room.send_to_room(player, message_to_send)
