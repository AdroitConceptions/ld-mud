from __future__ import print_function
NAME = 'get'

HELP_TEXT = 'get <x> pick something up'


def run(player, data):
    if data in [x['name'] for x in player.current_room.items]:
        for i in range(len(player.current_room.items)):
            if(player.current_room.items[i]['name'] == data):
                item = player.current_room.items.pop(i)
                player.account_info['items'].append(item)
                player.send_message("you pick up a {}\n".format(data))
                player.current_room.send_to_room(
                    player, "{0} picks up a {1}.\n".format(player.username, data))
                break
    else:
        player.send_message(
            "There doesn't seem to be a {} here.\n".format(data))
