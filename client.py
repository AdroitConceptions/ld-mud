from __future__ import print_function
from character_serialization import save_character


class Client:
    def __init__(self, server, conn, account_info, room):
        self.server = server
        self.conn = conn
        self.account_info = account_info
        self.username = account_info['username']
        self.input_buffer = ''
        self.connected = True

        self.current_room = room
        room.enter(self)

    def heartbeat(self):
        if self.account_info['health'] < 100:
            self.account_info['health'] += 1
        self.account_info['status_effects'][:] = [
            x for x in self.account_info['status_effects'] if x[1] > 0]
        for effect in self.account_info['status_effects']:
            effect[1] -= 1

    def run(self):
        while self.connected:
            try:
                message = self.conn.recv(2048)
                if message:
                    self.input_buffer += message
                    if '\n' in self.input_buffer:
                        split = self.input_buffer.split('\n', 1)
                        self.input_buffer = split[1]
                        self.parse_input(split[0])
                else:
                    self.disconnect()

                if len(message) > 2048:
                    self.input_buffer = ''
                    self.conn.send("Input To Long, purging\n")
            except Exception as e:
                print(self.username + " encountered an error: " + str(e))
                self.disconnect()

    def disconnect(self):
        self.current_room.send_to_room(
            self, "{0} vanishes into thin air\n".format(self.username))
        self.current_room.players.remove(self)
        self.connected = False
        save_character(self.account_info)
        self.conn.close()

    def parse_input(self, command):
        cmd = command.split()
        if len(cmd) == 0:
            return
        if(cmd[0] in self.current_room.exits()):
            self.current_room.exit(self, cmd[0])
        else:
            found_command = False
            for item in self.server.commands:
                if cmd[0] == item.NAME:
                    item.run(self, cmd[1] if len(cmd) > 1 else None)
                    found_command = True
                    break

            if not found_command:
                self.send_message("Unknown Command: " +
                                  cmd[0] + "\nType help for help\n")

    def send_message(self, message):
        try:
            self.conn.send(message + self.input_buffer)
        except:
            self.conn.close()
