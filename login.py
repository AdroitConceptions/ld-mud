from character_serialization import read_character

WELCOME_MESSAGE = """Welcome.

Please Note that this game uses a non-encrypted connection.
That means, do not submit any private information and do NOT
use a password that you use with any other system.

Please note, if you loose your username and password,
They are not recoverable.

What password would you like to use for your account?
"""


def login(conn, server):
    x = Login(conn, server)
    return x.process()


class Login:
    def __init__(self, conn, server):
        self.conn = conn
        self.in_buff = ''
        self.server = server

    def read_next_line(self):
        # return string of data, else return None.
        # while bock is broken by return statements
        while True:
            try:
                message = self.conn.recv(2048)
                if message:
                    self.in_buff += message
                    if '\n' in self.in_buff:
                        split = self.in_buff.split('\n', 1)
                        self.in_buff = split[1]
                        if split[0] != "":
                            return split[0].strip()
                else:
                    return None
            except Exception as e:
                return None

    def process(self):
        self.conn.send("Welcome to the Ludum Dare MUD\n\n")

        account_info = self.get_account()
        if account_info is not None:
            message = "\nWelcome {0}, enjoy your stay!\n\n".format(
                account_info['username'])
            self.conn.send(message)

        return account_info

    def get_account(self):
        while True:
            name = ' '
            while ' ' in name:
                self.conn.send("What is your name [No Spaces]?\n")
                name = self.read_next_line()

            if self.server.lock_name(name):
                try:
                    if self.server.is_logged_in(name):
                        self.conn.send("That user is already connected.\n")
                    else:
                        user_data = self.load_user_file(name)
                        if user_data == {}:
                            user_data = self.create_user(name)
                        if user_data is not None:
                            return user_data
                finally:
                    self.server.release_name(name)
            else:
                self.conn.send("That user is already connected.\n")

    def load_user_file(self, name):
        account_info = read_character(name)
        if account_info is None:
            return {}
        self.conn.send("Password:\n")
        password = self.read_next_line()
        if password == account_info['password']:
            return account_info
        self.conn.send("Invalid Account Info:\n")
        return None

    def create_user(self, name):
        response = None
        while response not in ['yes', 'no']:
            self.conn.send(
                "{0}, you say?  Are you new here? [yes/no]\n".format(name))
            response = self.read_next_line()
            if response in [None, 'no']:
                return None
        self.conn.send(WELCOME_MESSAGE)
        password = self.read_next_line()

        account_info = {
            'username': name,
            'password': password,
            'level': 1,
            'skills': [],
            'items': [],
            'health': 100,
            'experiance': 0,
            'status_effects': []
        }

        return account_info
