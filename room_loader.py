from __future__ import print_function

import yaml
import os
from room import Room


def load_rooms():
    rooms = {}
    for r, d, f in os.walk('rooms'):
        for file in f:
            if '.yml' in file:
                with open(os.path.join(r, file)) as data:
                    rooms[int(file[:-4])] = Room(
                        yaml.load(data.read(), Loader=yaml.FullLoader),
                        # yaml.load(data.read()),
                        rooms)

    return rooms
