from __future__ import print_function

from server import Server


def main():
    print("## Server Start")
    server = Server('', 8081, 100)
    server.run()


if __name__ == "__main__":
    main()
